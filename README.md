# k8s-service-2

This repository is used for deploying microservice k8s-service-2. It is possible txo use files from this directory to deploy service.


## Regex option

If you need to change the value of regex parameter, it's necessary to edit Simloudfile.yaml.
There are 2 possible options for regex value:
- true - for service, the regex rules will be applied based on the already set and specifically configured regex rules in Simloudfile.yaml. It could be a custom value.
- false - service will be deployed without any regex rules and according to already specified configuration in URL path.

Example of code block:

```
regex:
    enabled: false
    rewrite-target: /$2$3$4
````
```
regex:
    enabled: true
    rewrite-target: /$2$3$4
```

## Passing secrets from vault via simloid_ci.sh file

Follow these steps to output vault secrets through a simloud_ci.sh file in a Jenkins job:

Add the following commands to your simloud_ci file, where <path_to_secret> is a path to required secret in vault:

```sh
  username=$(vault kv get -field=username  <path_to_secret>)
  password=$(vault kv get -field=password  <path_to_secret>)
```
You can view the values by navigating to the job build output after building the service from the branch.

>NOTE: It is possible to provide custom steps using simloud_ci file.


## Additional options for cloud_resources block

With Simloudfile.yaml functionality is also possible to deploy Databases.

It is possible to deploy DynamoDB database using Simloudfile.yaml from kube-service-2 repository.

For deploying **DynamoDB**, it is necessary to add following code snippet to `cloud_resources` block at Simloudfile.yaml.

```yaml
- name: dynamodb_1_ks2
    env_name_prefix: ENVDB2
    type: dynamodb
    params:
      AttributeDefinitions:
        - AttributeName: username
          AttributeType: S
        - AttributeName: lastname
          AttributeType: S
      KeySchema:
        - AttributeName: username
          KeyType: HASH
        - AttributeName: lastname
          KeyType: RANGE
```
For more information about deploying DynamoDB database, please, follow this link <a href="https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/dynamodb.html#DynamoDB.Client.create_table" target="_blank">DynamoDB</a>.


>NOTE: It is possible to deploy k8s-service-2 with additional options for cloud_resources block in Simloudfile.yaml, such as `S3` and `SQS`.

For deploying **S3**, it is necessary to add following code snippet to `cloud_resources` block at Simloudfile.yaml:

```yaml
- name: s3_1
  env_name_prefix: S31
  type: s3

```
S3 segment is parsed using the S3 module from the boto. More information <a href="https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/s3.html#S3.Client.create_bucketz" target="_blank">here</a>.

For deploying **SQS**, it is necessary to add following code snippet to `cloud_resources` block at Simloudfile.yaml:

```yaml
 - name: kube-service2.sqs_2
   env_name_prefix: SQS2
   type: sqs
```
SQS segment is parsed using the SQS module from the boto. More information <a href="https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/sqs.html#SQS.Client.create_queue" target="_blank">here</a>.


You can see the snippets of code for deploying these services in [Simloudfile.yaml](https://gitlab.com/simloud-demo/k8s-service-2/-/blob/main/Simloudfile.yaml) in this repository or by the following <a href="https://prod--simloud-docs.netlify.app/en/examples-of-simloud-files/#creating-and-deploying-databases" target="_blank">link</a>.


**Additional documentation is placed by links:**
- [**"Simloudfile.yaml"**](https://docs.simloud.com/en/simloudfile.yaml/)

- [**"How to use Simloud files"**](https://docs.simloud.com/en/how-to-use-simloud-files/)

- [**"How to create and manage your SSH keys"**](https://docs.simloud.com/en/getting-started/#managing-the-ssh-keys)

- [**"How to work with repositories"**](https://docs.simloud.com/en/getting-started/#add-new-git-repositories-services)

