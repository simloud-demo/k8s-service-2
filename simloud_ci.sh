#!/bin/bash
docker image build --network host  -t $DOCKER_IMAGE_NAME -f Dockerfile .

username=$(vault kv get -field=username  jenkins/test/config)
password=$(vault kv get -field=password  jenkins/test/config)

echo Username: $username
echo Password: $password

